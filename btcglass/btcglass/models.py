from django.db import models

import requests

from django.contrib.auth.models import User

BLOCKCHAIN_API_WALLET = "https://blockchain.info/address/{wallet}?format=json"

class Wallet(models.Model):
    owner = models.ForeignKey(User)
    addr = models.CharField(max_length=128)
    min_in = models.DecimalField(max_digits=9, decimal_places=8, default=0)
    min_out = models.DecimalField(max_digits=9, decimal_places=8, default=0)

    last_transaction = models.IntegerField(default=0)

    def __unicode__(self):
        return u'%s %s' % (self.owner, self.addr)

    def get_all_transactions(self):
        txs = []
        res = requests.get(BLOCKCHAIN_API_WALLET.format(wallet=self.addr)).json()
        for tx in res['txs']:
            for part in tx['out']:
                if part['addr'] == self.addr:
                    txs.append(('in', tx['time'], part['value']))
        return txs

    def get_new_transactions(self):
        new_txs = []
        txs = self.get_all_transactions()
        for tx in txs:
            if tx[1] > self.last_transaction:
                new_txs.append(tx)
        if len(txs):
            self.last_transaction = txs[0][1]
            self.save()
        return new_txs


