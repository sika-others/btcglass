from django.conf import settings

from authomatic.providers import oauth2

AUTHOMATIC = {
    'google': {
        'class_': oauth2.Google,
        'consumer_key': settings.MIRROR_API_CLIENT_ID,
        'consumer_secret': settings.MIRROR_API_CLIENT_SECRET,
        'scope': ['email', ]
    },
}