from django.contrib import admin

from .models import Wallet


class WalletAdmin(admin.ModelAdmin):
    list_display = ('owner', 'addr', 'min_in', 'min_out', 'last_transaction')
    list_filter = list_display

admin.site.register(Wallet, WalletAdmin)
