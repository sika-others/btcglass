from django.core.management import BaseCommand
from btcglass.models import Wallet

class Command(BaseCommand):
    help = u"Send notifications for new transactions"
    def handle(self, *args, **options):
       for wallet in Wallet.objects.all():
           wallet.notify()
