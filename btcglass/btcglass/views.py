from django.http import HttpResponse
from django.conf import settings

from authomatic import Authomatic
from authomatic.adapters import DjangoAdapter

from config import AUTHOMATIC

authomatic = Authomatic(AUTHOMATIC, settings.SECRET_KEY)

def home(request):
    # Create links and OpenID form to the Login handler.
    return HttpResponse('''
        Login with <a href="login/fb">Facebook</a>.<br />
        Login with <a href="login/tw">Twitter</a>.<br />
        <form action="login/oi">
            <input type="text" name="id" value="me.yahoo.com" />
            <input type="submit" value="Authenticate With OpenID">
        </form>
    ''')

def login(request, provider_name):
    response = HttpResponse()
    result = authomatic.login(DjangoAdapter(request, response), provider_name)
    if result:
        response.write('<a href="..">Home</a>')
        if result.error:
            response.write('<h2>Damn that error: {}</h2>'.format(result.error.message))
        elif result.user:
            if not (result.user.name and result.user.id):
                result.user.update()
            response.write('<h1>Hi {}</h1>'.format(result.user.name))
            response.write('<h2>Your id is: {}</h2>'.format(result.user.id))
            response.write('<h2>Your email is: {}</h2>'.format(result.user.email))
    return response